package com.apps.daken0915.qrl;

import android.app.Activity;
import android.content.ClipboardManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.Hashtable;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        String url;

        if(cm.getText() == null){
            url = "http://google.com";
        }else{
            url = cm.getText().toString();
        }

        Bitmap bm = createQRCode(url, 500, 500);

        Button btn = (Button) findViewById(R.id.app_button);
        btn.setOnClickListener(this);

        ImageView img = (ImageView) findViewById(R.id.result_view);
        img.setImageBitmap(bm);

    }

    @Override
    public void onResume(){
        super.onResume();
        ClipboardManager cm = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        String url;

        if(cm.getText() == null){
            url = "http://google.com";
        }else{
            url = cm.getText().toString();
        }

        Bitmap bm = createQRCode(url, 500, 500);

        ImageView img = (ImageView) findViewById(R.id.result_view);
        img.setImageBitmap(bm);
    }

    public void onClick(View v){
        Intent intent = new Intent();
        intent.setClassName("com.apps.daken0915.qrl", "com.apps.daken0915.qrl.ListActivity");
        startActivity(intent);
    }

    public static Bitmap createQRCode(String content, int width, int height) {
        Bitmap bitmap = null;
        try {
            BarcodeFormat format = BarcodeFormat.QR_CODE;

            Hashtable hints = new Hashtable();
            hints.put(EncodeHintType.CHARACTER_SET, "utf8");
            hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);

            QRCodeWriter writer = new QRCodeWriter();
            BitMatrix bitMatrix = writer.encode(content, format, width, height, hints);

            int[] pixels = new int[width * height];
            // データがあるところだけ黒にする
            for (int y = 0; y < height; y++) {
                int offset = y * width;
                for (int x = 0; x < width; x++) {
                    pixels[offset + x] = bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE;
                }
            }
            bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bitmap;
    }
}
